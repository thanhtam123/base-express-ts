import ApiError from 'utils/ApiError';
import HttpStatus from 'utils/httpStatusCode';

export class NotFoundException extends ApiError {
	constructor(message: string) {
		super(HttpStatus.NOT_FOUND, message);
	}
}
