import { NextFunction, Request, Response } from 'express';
import ApiError from '../utils/ApiError';
import StatusCodes from '../utils/httpStatusCode';

// conver error to ApiError
export const errorConverter = (
	err: any,
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	let apiErr = err;
	if (!(apiErr instanceof ApiError)) {
		const statusCode =
			apiErr.statusCode || apiErr.name === 'MongoError'
				? StatusCodes.BAD_REQUEST
				: StatusCodes.INTERNAL_SERVER_ERROR;
		const message = apiErr.message || StatusCodes[statusCode];
		apiErr = new ApiError(statusCode, message, false, err.stack);
	}
	next(apiErr);
};

// handle error from catchAsync
export const errorHandler = async (
	apiErr: ApiError,
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	let { statusCode, message, stack } = apiErr;
	const response = {
		code: statusCode,
		message,
		stack: stack,
	};
	res.status(statusCode).send(response);
};
