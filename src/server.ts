import App from './app';
import 'dotenv/config';
import envValidate from './utils/validateEnv';
import controllerInterface from './interfaces/controller.interface';

envValidate();

const controllers: controllerInterface[] = [];

const app = new App(controllers);
app.listen();
