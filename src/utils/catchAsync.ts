import { Request, Response, NextFunction } from 'express';

interface optionsInterface {
	statusCode?: number;
}

const catchAsync =
	(fn: Function, opts: optionsInterface = {}) =>
	(req: Request, res: Response, next: NextFunction) => {
		Promise.resolve(fn(req, res, next))
			.then((result: object) => {
				const statusCode: number = opts.statusCode
					? opts.statusCode
					: 200;
				res.status(statusCode).send(result);
			})
			.catch((err) => next(err));
	};

export default catchAsync;
