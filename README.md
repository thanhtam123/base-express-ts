## Description

This is a project template that consist base code for projects using express and typescript.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run dev

# production
$ npm start
```
